package com.giorgi.sorting;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
@Category(TenItemsCaseTest.class)
public class TenItemsCaseTest {

    final private int[] arrayBefore;
    final private int[] arrayAfter;

    public TenItemsCaseTest(int[] arrayBefore, int[] arrayAfter) {
        this.arrayBefore = arrayBefore;
        this.arrayAfter = arrayAfter;
    }

    @Parameters
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                {new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
                        new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}},

                {new int[]{3, -16, 0, 98, 40, 23, 28, 67, -12, 8},
                        new int[]{-16, -12, 0, 3, 8, 23, 28, 40, 67, 98}},

                {new int[]{57, 20, 0, -4, -2, -9, 6, 4, 3, 1},
                        new int[]{-9, -4, -2, 0, 1, 3, 4, 6, 20, 57}},

                {new int[]{29, 1, 3, 0, 20, 18, 7, 3, 4, 2},
                        new int[]{0, 1, 2, 3, 3, 4, 7, 18, 20, 29}}
        });
    }

    @Test
    public void testTenItemsCase() {
        int[] sorted = Sorting.sort(arrayBefore);
        Assert.assertArrayEquals(sorted, arrayAfter);
    }

}
