package com.giorgi.sorting;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class MoreThanTenCaseTest {
    final private int[] arrayBefore;
    final private int[] arrayAfter;

    public MoreThanTenCaseTest(int[] arrayBefore, int[] arrayAfter) {
        this.arrayBefore = arrayBefore;
        this.arrayAfter = arrayAfter;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                {new int[]{9, 8, 7, 6, 5, 4, 3, 1, 0},
                        new int[]{0, 1, 3, 4, 5, 6, 7, 8, 9}},

                {new int[]{3, -16, 23, 28, 67, -12, 8},
                        new int[]{-16, -12, 3, 8, 23, 28, 67,}},

                {new int[]{57, 20, 0, 1},
                        new int[]{0, 1, 20, 57}},

                {new int[]{29, 1, 3, 0, 20,},
                        new int[]{0, 1, 3, 20, 29}}
        });
    }

    @Test
    public void testLessThanTenCase() {
        int[] sorted = Sorting.sort(arrayBefore);
        Assert.assertArrayEquals(sorted, arrayAfter);
    }

}
