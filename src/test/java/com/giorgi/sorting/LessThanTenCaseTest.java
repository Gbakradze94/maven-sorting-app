package com.giorgi.sorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)

public class LessThanTenCaseTest {

    final private int[] arrayBefore;
    private int[] arrayAfter;
    private String exceptionMessage;

    public LessThanTenCaseTest(String ExceptionMessage, int[] arrayBefore, int[] arrayAfter) {
        this.arrayBefore = arrayBefore;
        this.arrayAfter = arrayAfter;
        this.exceptionMessage = ExceptionMessage;
    }

    @Parameters
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                {"More than 10 items were entered", new int[]{9, 8, 7, 6, 5, 4, 3, 1, 0, 13, 19},
                        new int[]{9, 8, 7, 6, 5, 4, 3, 1, 0, 13, 19}},
                {"More than 10 items were entered", new int[]{54, 3, -4, 20, 97, 0, 34, 122, 98, 13, 19, 48, 80},
                        new int[]{54, 3, -4, 20, 97, 0, 34, 122, 98, 13, 19, 48, 80}},
                {"More than 10 items were entered", new int[]{-15, -3, -78, 67, 97, 0, 34, 38, 78, 11, 8, 31, -5, 22},
                        new int[]{-15, -3, -78, 67, 97, 0, 34, 38, 78, 11, 8, 31, -5, 22}},
                {"More than 10 items were entered", new int[]{6, 4, 2, 78, 7, 15, 19, 33, 45, 29, 24, 39, 55, -6, 17},
                        new int[]{6, 4, 2, 78, 7, 15, 19, 33, 45, 29, 24, 39, 55, -6, 17}}
        });
    }

    @Test
    public void testLessThanTenCase() {
        arrayAfter = Sorting.sort(arrayBefore);
        assertArrayEquals("More than 10 items were entered", arrayAfter, arrayBefore);
    }
}
