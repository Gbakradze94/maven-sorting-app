package com.giorgi.sorting;

import org.junit.Assert;
import org.junit.Test;

public class ZeroArrayCaseTest {

    int[] arrayBefore = { };
    int[] arrayAfter;

    @Test
    public void testZeroArrayCase() {
        int[] arrayAfter = Sorting.sort(arrayBefore);
        Assert.assertArrayEquals("Zero arguments passed", arrayAfter, arrayBefore);
    }
}
