package com.giorgi.sorting;


import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;


public class App {
    public static void main(String[] args) {
        System.out.println("Enter up to 10 integers to  sort: ");
        try {
            Scanner scanner = new Scanner(System.in).useDelimiter("[,\\s+]");

            int n = 0;
            int i = 0;
            int[] array = new int[10];

            while (scanner.hasNextInt()) {
                array[i] = scanner.nextInt();
                i++;
                n++;
            }

            int[] newArray = Arrays.copyOf(array,n);


            if (newArray.length == 0) {
                System.out.println("Zero arguments passed");
            }
            if (newArray.length < 11 && newArray.length > 0) {
                int[] sortedArray = Sorting.sort(newArray);
                System.out.println("Sorted Elements Are: ");
                for (int j : sortedArray
                ) {
                    System.out.print(" " + j + " ");
                }
            }
            if (newArray.length > 11) {
                System.out.println("More than 10 arguments were passed");
            }


        } catch(
                IndexOutOfBoundsException e) {
            System.out.print("More than 10 items were entered");
        } catch (InputMismatchException e) {
            System.out.print("Value is either empty or not a number");

        }


    }

}
