package com.giorgi.sorting;


public class Sorting {
    public static int[] sort(int[] array) {

        int n = array.length;
        int temp;
        if (n == 0) {
            System.out.println("Zero arguments passed");
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (array[j + 1] < array[j]) {
                    temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }
}
